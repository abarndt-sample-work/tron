/**************************************************************************************************/
/* Author: Andrew Barndt                                                                          */
/* Description: A C program that controls the artificial intelligence for a ten-member formation  */
/* game of tron. Its strategy is to look ahead in each possible direction to see how many open    */
/* spaces there are, and then chooses the move that results in the most possible open spaces      */
/* possible after the move.                                                                       */
/*************************************************************************************************/

#include <stdio.h>
#include <string.h>
#include "tron.h"

#define MAXDEPTH 11

//Array to represent the board the MCP uses
static unsigned char board[MAX_GRID_HEIGHT][MAX_GRID_WIDTH];

static int hasPlayed = FALSE, playerIndex = 0;
static int width, height, numOfCycles = 0;
static int space = 0;

//Struct to hold the info for each cycle. Contains name, coordinates, if it's jumped, if it's alive,
//if it was alive last move, and a list of all the coordinates the trail is in. Has AB at the end of
//its name to ensure that if another person used a struct named cycleInfo that there's no conflict.
struct CycleInfoAB
{
    char letter;
    short x, y;
    short hasJump;
    short isAlive;
    short aliveLastTime;
    int xCoordinates[(MAX_GRID_HEIGHT - 2) * (MAX_GRID_WIDTH - 2)];
    int yCoordinates[(MAX_GRID_HEIGHT - 2) * (MAX_GRID_WIDTH - 2)];
};

//Arrays of structs to hold info for each of the 10 cycles
static struct CycleInfoAB myCycles[MAX_CYCLES_PER_PLAYER];
static struct CycleInfoAB oppCycles[MAX_CYCLES_PER_PLAYER];

//Function prototypes
static int getOpenDirections(int x, int y);
static int getJumpCount (int x, int y);
static int getOpenSpaces (int x, int y, int depth);
static void bubbleSort(int arr[], int arrSize);
static void printBoard();

/**************************************************************************************************/
/* Very simple function that takes no parameters and simply returns a character array of the      */
/* player's chosen name.                                                                          */
/**************************************************************************************************/

char* Andrew_Barndt_getName()
{
    return "Chick-fil-a";
}

/**************************************************************************************************/
/* Takes as a parameter a struct holding initial game data, and uses that to fill in the 2D array */
/* representation of the game board. Sets up the board with the border and walls in the           */
/* appropriate locations.                                                                         */
/**************************************************************************************************/

void Andrew_Barndt_init(struct InitData *data)
{
    width = (*data).gridWidth;
    height = (*data).gridHeight;
    
    if (strcmp("Chick-fil-a", (*data).name[0]) == 0) playerIndex = 0;
    else playerIndex = 1;
    
    for (int i = 0; i < width; i++) 
    { 
        board[0][i] = 255;
        board[height - 1][i] = 255;
    }
    for (int i = 0; i < height; i++)
    {
        board[i][0] = 255;
        board[i][width - 1] = 255;
    }

    for (int i = 1; i < width - 1; i++)
    {
        for (int j = 1; j < height - 1; j++)
        {
            board[j][i] = 0;
        }
    }    
    
    struct Wall *temp = (*data).wallList;
    while (temp != NULL)
    {
        board[(*temp).y][(*temp).x] = 255;
        temp = (*temp).next;
    }
}

/**************************************************************************************************/
/* Function to coordinate moves of each cycle. At the start of the function, the board is updated */
/* with the locations of the latest moves, then each location is put into its corresponding       */
/* cycleInfo struct. Then we check to see if the cycle was killed after the last set of moves, and*/
/* erase its trail from the board. Then it finally can do the calculations to decide where to move*/
/* next - first checking if it has no available moves, and if so then checks for an available jump*/
/* and takes the move with the most open spaces after jumping. If there's only one open move, it  */
/* simply takes that. If there's more than one open move available, it calculates which one has   */
/* the most empty spaces remaining after moving there and moves there.                            */
/**************************************************************************************************/

void Andrew_Barndt_move(struct MoveData *data)
{
    struct Cycle *starting = (*data).cycleList;
    int counter = 0;

    //Initial loop to update the board with the latest moves and, if it's the first turn, initialize
    //some of the fields of each cycleInfo struct such as letter
    while (starting != NULL)
    {
        board[(*starting).y][(*starting).x] = (*starting).cycleName;
        //Do initial move bookkeeping
        if ((*data).turnNumber == 1)
        {
            switch(playerIndex)
            {
                //For whatever reason, the letters would always be 10 too high so a -10 statement
                //becomes necessary
                case 0: myCycles[counter % 10].letter = 'A' + counter - 10;
                        oppCycles[counter % 10].letter = 'K' + counter - 10; break;
                case 1: myCycles[counter % 10].letter = 'K' + counter - 10;
                        oppCycles[counter % 10].letter = 'A' + counter - 10; break;
            }
            //x, y, and jump initialization
            if ((!playerIndex && counter < 10) || (playerIndex && counter > 9))
            {
                myCycles[counter % 10].xCoordinates[0] = (*starting).x;
                myCycles[counter % 10].yCoordinates[0] = (*starting).y;
                myCycles[counter % 10].aliveLastTime = TRUE;
                myCycles[counter % 10].hasJump = FALSE;
            }
            else
            {
                oppCycles[counter % 10].xCoordinates[0] = (*starting).x;
                oppCycles[counter % 10].xCoordinates[0] = (*starting).y;
                oppCycles[counter % 10].aliveLastTime = TRUE;
            }
        }
        starting = (*starting).nextCycle;
        counter++;
    }
    
    //Add each new move here into the x-y arrays; loop over them with the cycle struct. This basically
    //just takes the head location of the cycle and puts its location into the corresponding coordinate arrays.
    for (int i = 0; i < MAX_CYCLES_PER_PLAYER; i++)
    {
        starting = (*data).cycleList;
        while (starting != NULL)
        {
            if (myCycles[i].isAlive && (*starting).cycleName == myCycles[i].letter)
            {
                myCycles[i].xCoordinates[(*data).turnNumber - 1] = (*starting).x;
                myCycles[i].yCoordinates[(*data).turnNumber - 1] = (*starting).y;
            }
            else if (oppCycles[i].isAlive && (*starting).cycleName == oppCycles[i].letter)
            {
                oppCycles[i].xCoordinates[(*data).turnNumber - 1] = (*starting).x;
                oppCycles[i].yCoordinates[(*data).turnNumber - 1] = (*starting).y;
            }
            starting = (*starting).nextCycle;
        }
    }
    
    /*
    For bookkeeping, we need to update each cycle. This means checking if it's still alive first of all.
    We do this by setting all cycles equal to dead, then iterate over each cycle in the linked list and set
    those to alive. If a cycle is alive, then add its latest move to the x and y coordinates. If it's dead,
    loop through the x and y coordinates of the cycle 
    */
    //Set all to 0
    for (int i = 0; i < MAX_CYCLES_PER_PLAYER; i++)
    {
        myCycles[i].isAlive = FALSE;
        oppCycles[i].isAlive = FALSE;
    }

    //Check each index in both players' arrays to see if the linked list of cycles has their name;
    //if it does, that means they're alive
    struct Cycle *checker1;
    counter = 0;
    for (char c = 'A'; c < 'K'; c++)
    {
        checker1 = (*data).cycleList;
        short hold = 0;
        while (checker1 != NULL)
        {
            if ((*checker1).cycleName == c)
            {
                hold = 1;
                break;
            }
            checker1 = (*checker1).nextCycle;
        }
        switch(playerIndex)
        {
            case 0: if (hold) myCycles[counter].isAlive = TRUE; break;
            case 1: if (hold) oppCycles[counter].isAlive = TRUE; break;
        }
        counter++;
    }

    struct Cycle *checker2;
    counter = 0;
    for (char c = 'K'; c < 'U'; c++)
    {
        checker2 = (*data).cycleList;
        short hold = 0;
        while (checker2 != NULL)
        {
            if ((*checker2).cycleName == c)
            {
                hold = 1;
                break;
            }
            checker2 = (*checker2).nextCycle;
        }
        switch(playerIndex)
        {
            case 0: if (hold) oppCycles[counter].isAlive = TRUE; break;
            case 1: if (hold) myCycles[counter].isAlive = TRUE; break;
        }
        counter++;
    }
    
    //Destroy trails of those who just died
    for (int i = 0; i < MAX_CYCLES_PER_PLAYER; i++)
    {
        if (oppCycles[i].isAlive != oppCycles[i].aliveLastTime)
        {
            for (int j = 0; j < (*data).turnNumber; j++)
            {
                board[oppCycles[i].yCoordinates[j]][oppCycles[i].xCoordinates[j]] = 0;
            }
            oppCycles[i].aliveLastTime = 0;
        }
        if (myCycles[i].isAlive != myCycles[i].aliveLastTime)
        {
            //printf("%c died last turn", myCycles[i].letter);
            for (int j = 0; j < (*data).turnNumber; j++)
            {
                board[myCycles[i].yCoordinates[j]][myCycles[i].xCoordinates[j]] = 0;
            }
            myCycles[i].aliveLastTime = 0;
        }
    }

    //Now we can move - check first if one can jump, and if not, clear the player's trail
    struct Cycle *moving = (*data).cycleList;
    while (moving != NULL)
    {
        //Set up bookkeeping with variables to hold the current values of the array index of the cycle
        //the program is at, along with its x and y coordinates, plus the variables to hold the number
        //of open spaces in each direction
        int currentIndex;
        char currentName = (*moving).cycleName;
        for (int i = 0; i < MAX_CYCLES_PER_PLAYER; i++)
        {
            if (myCycles[i].letter == currentName)
            {
                currentIndex = i;
                break;
            }
        }
        short currentX = (*moving).x;
        short currentY = (*moving).y;
        int northSpaces = 0, eastSpaces = 0, southSpaces = 0, westSpaces = 0;
        int spaceChoices[4];

        //Only make a move if it's one of the player's cycles and not the opponent's
        if ((playerIndex == 0 && currentName < 'K') || (playerIndex == 1 && currentName > 'J'))
        {
            int numOfDirections = getOpenDirections(currentX, currentY);
            //printf("%c has %d open directions\n", currentName, numOfDirections);
            
            //If no open directions, check to see if one can do a jump and do it if possible
            if (numOfDirections == 0 && myCycles[currentIndex].hasJump == FALSE)
            {
                int jumpCount = getJumpCount(currentX, currentY);
                //printf("possible jumps: %d\n", jumpCount);
                if (jumpCount > 0)
                {
                    //printf("%c jumps\n", currentName);
                    //Get the number of spaces after each jump and choose the one with the most open spaces
                    if (currentY > 1) northSpaces = getOpenSpaces(currentX, currentY - 2, 0);
                    if (currentX < width - 2) eastSpaces = getOpenSpaces(currentX + 2, currentY, 0);
                    if (currentY < height - 2) southSpaces = getOpenSpaces(currentX, currentY + 2, 0);
                    if (currentX > 1) westSpaces = getOpenSpaces(currentX - 2, currentY, 0);
                    spaceChoices[0] = northSpaces;
                    spaceChoices[1] = eastSpaces;
                    spaceChoices[2] = southSpaces;
                    spaceChoices[3] = westSpaces;
                    bubbleSort(spaceChoices, 4);

                    if (spaceChoices[3] == northSpaces)
                    {
                        (*moving).y -= 2;
                        board[(*moving).y][currentX] = currentName;
                    }
                    else if (spaceChoices[3] == eastSpaces)
                    {
                        (*moving).x += 2;
                        board[currentY][(*moving).x] = currentName;
                    }
                    else if (spaceChoices[3] == southSpaces)
                    {
                        (*moving).y += 2;
                        board[(*moving).y][currentX] = currentName;
                    }
                    else
                    {
                        (*moving).x -= 2;
                        board[currentY][(*moving).x] = currentName;
                    }

                    myCycles[currentIndex].hasJump = TRUE;
                }
                else //Simply let the cycle die, and set the isAlive variable to false, if no adjacent spaces
                     //or jumps are available
                {
                    for (int i = 0; i < MAX_CYCLES_PER_PLAYER; i++)
                    {
                        switch(playerIndex)
                        {
                            case 0: if (myCycles[i].letter < 'K' && myCycles[i].letter == currentName)
                                    { myCycles[i].isAlive = FALSE;
                                    }
                                    break;
                            case 1: if (myCycles[i].letter > 'J' && myCycles[i].letter == currentName)
                                    { myCycles[i].isAlive = FALSE;
                                    }
                        }
                    }
                }
            }
            
            //If only one direction is open, move there
            else if (numOfDirections == 1)
            {
                if (board[currentY - 1][currentX] == 0)
                {
                    (*moving).y--;
                    board[(*moving).y][currentX] = currentName;
                }
                else if (board[currentY + 1][currentX] == 0)
                {
                    (*moving).y++;
                    board[(*moving).y][currentX] = currentName;
                }
                else if (board[currentY][currentX - 1] == 0)
                {
                    (*moving).x--;
                    board[currentY][(*moving).x] = currentName;
                }
                else
                {
                    (*moving).x++;
                    board[currentY][(*moving).x] = currentName;
                }
            }

            //If more than one direction is open, see which direction has the most free space available
            else
            {
                if (board[currentY - 1][currentX] == 0) northSpaces = getOpenSpaces(currentX, currentY - 1, 0);
                if (board[currentY][currentX + 1] == 0) eastSpaces = getOpenSpaces(currentX + 1, currentY, 0);
                if (board[currentY + 1][currentX] == 0) southSpaces = getOpenSpaces(currentX, currentY + 1, 0);
                if (board[currentY][currentX - 1] == 0) westSpaces = getOpenSpaces(currentX - 1, currentY, 0);
                spaceChoices[0] = northSpaces;
                spaceChoices[1] = eastSpaces;
                spaceChoices[2] = southSpaces;
                spaceChoices[3] = westSpaces;
                //printf("%d, %d, %d, %d\n", spaceChoices[0], spaceChoices[1], spaceChoices[2], spaceChoices[3]);
                bubbleSort(spaceChoices, 4); //Sort the choices to always choose the largest one
                if (spaceChoices[3] == northSpaces)
                {
                    (*moving).y--;
                    board[(*moving).y][currentX] = currentName;
                }
                else if (spaceChoices[3] == eastSpaces)
                {
                    (*moving).x++;
                    board[currentY][(*moving).x] = currentName;
                }
                else if (spaceChoices[3] == southSpaces)
                {
                    (*moving).y++;
                    board[(*moving).y][currentX] = currentName;
                }
                else
                {
                    (*moving).x--;
                    board[currentY][(*moving).x] = currentName;
                }
            }
        }
        moving = (*moving).nextCycle;
    }
   // printBoard();
}

/**************************************************************************************************/
/* Calculates the number of open directions a cell has around it. Takes x and y coordinates and   */
/* counts the number of open cells around it, and returns that count as an int.                   */
/**************************************************************************************************/

int getOpenDirections(int x, int y)
{
    int count = 0;
    if (board[y - 1][x] == 0) count++;
    if (board[y + 1][x] == 0) count++;
    if (board[y][x - 1] == 0) count++;
    if (board[y][x + 1] == 0) count++;
    return count;
}

/**************************************************************************************************/
/* If a cell has no open directions around it, this function is called with the x and y           */
/* coordinates of the cell and sees if there's a cell to jump two spots to, and returns the int   */
/* for the possible number of jumps.                                                              */
/**************************************************************************************************/

static int getJumpCount (int x, int y)
{
    int jumps = 0;
    if (y > 1 && board[y - 2][x] == 0) jumps++;
    if (y < height - 2 && board[y + 2][x] == 0) jumps++;
    if (x > 1 && board[y][x - 2] == 0) jumps++;
    if (x < width - 2 && board[y][x + 2] == 0) jumps++;
    return jumps;
}

/**************************************************************************************************/
/* Heart of the AI calculations. Takes the x and y coordinates of the given location, and returns */
/* 0 if the space is occupied. Otherwise, marks the spot on the board with a 1 to ensure later    */
/* searches from the same original move (as in, checking to the north of where the cycle currently*/
/* is) don't double count spaces and count an errant number of open cells. Then it recursively    */
/* continues finding open spaces until it gets MAXDEPTH spaces from where the cycle is currently  */
/* (or encounters a dead end), at which point the function starts returning. It requires a 1 to   */
/* account for the current space, then adds the result of the later searches as the total, until  */
/* the function returns to the original depth and then we have the number of open spaces within   */
/* the specified depth.                                                                           */
/**************************************************************************************************/

static int getOpenSpaces (int x, int y, int depth)
{
    if (y > height - 1 || y < 0 || x > width - 1 || x < 0 || board[y][x] != 0 || depth == MAXDEPTH) return 0;
    board[y][x] = 1;
    int fillTotal = 1 + getOpenSpaces(x, y - 1, depth + 1) + getOpenSpaces (x, y + 1, depth + 1)
            + getOpenSpaces(x - 1, y, depth + 1) + getOpenSpaces(x + 1, y,  depth + 1);
    board[y][x] = 0;
    return fillTotal;
}

/**************************************************************************************************/
/* Standard bubble sort algorithm. Passes through an array arrSize times, swapping values if the  */
/* value in a given location in the array is higher than the next location.                       */
/**************************************************************************************************/

static void bubbleSort(int arr[], int arrSize)
{
    for (int i = 0; i < arrSize - 1; i++)
    {
        for (int j = 0; j < arrSize - i - 1; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

/**************************************************************************************************/
/* Prints out the board with a nested loop, giving each space five spaces to display a maximum    */
/* value of 255. Used for debugging but not called anywhere in the final product.                 */
/**************************************************************************************************/

static void printBoard()
{
    printf("\n");
    for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                printf("%5d", board[i][j]);
            }
            printf("\n");
        }
}