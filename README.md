# Tron
## Overview
This project was a one-file C program designed to work with the instructor's Master Control Program (as he called it) to pit two teams of 10 Tron bots against each other. The main rules were as follows:
* Each active bot moves one space per turn.
* Each bot may jump once per game; i.e. if all spaces in the cardinal directions are occupied, then the bot may "jump" two spaces over.
* All moves, regular or jumps, must be in one of the cardinal directions (no diagonal moves).
* If a bot is destroyed, its entire trail is eliminated too.
* A bot is destroyed if it runs into an occupied space, or doesn't move.
* The game is over when all of a team's bots have been destroyed.
* Each team's bots, along with things like the game's walls, were represented by the in the Master Control Program as a linked list of structs.

## Strategy
<p>The strategy I used for this project was that each bot's move should maximize the number of remaining moves remaining (within a given number of moves, in this case 11 moves beyond the current position). Basically: find the move with the most space around it. This algorithm was made quite simple with the use of a recursive function (getOpenSpaces) to determine how many spaces could be possibly moved into. The strategy was also used when no immediate moves were available and a jump was required, in which case the bot would jump to the space with the most possible moves remaining.</p>
<p>Below is the basic decision tree during each bot's move:</p>

* Check number of open directions
* If no open directions, check if there are any jumps
    * If a jump exists, take the jump with the most possible remaining moves
    * If no jump exists, do nothing since the bot will be destroyed regardless
* If only one open direction exists, move there
* If more than one open direction exists, move to where the most possible remaining moves are after moving to it

